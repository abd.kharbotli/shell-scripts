#!/bin/bash

# Function to validate the existence of a directory
validate_directory() {
  if [ ! -d "$1" ]; then
    echo "Error: Directory does not exist."
    exit 1
  fi
}

# Function to create the destination directory if it doesn't exist
create_destination_directory() {
  if [ ! -d "$1" ]; then
    mkdir -p "$1"
  fi
}

# Function to validate the desired backup count is a positive integer
validate_backups_number() {
  if ! [[ "$num_backups" =~ ^[0-9]+$ ]] || [ "$num_backups" -lt 1 ]; then
  echo "Error: Please enter a valid positive integer for the number of backups to keep."
  exit 1
  fi
}

# Function to perform the backup
perform_backup() {
  
  # Perform the temporary backup using tar
  tar -czf "$temp_backup_filename" -C "$source_directory" .
  tar_status=$?

  # Check the exit status to determine if the tar was successful
  if [ $tar_status -eq 0 ]; then
    backup_summary+="Compression completed successfully!"
    echo "$(date +'%b %d %H:%m:%S')"  "Compression completed successfully!" | tee -a "backup.log"
  else
    backup_summary+="Error: Compression failed with status code $tar_status."
    echo "$(date +'%b %d %H:%m:%S')"  "Error: Compression failed with status code $tar_status." | tee -a "backup.log"
    
  #echo "Error: Compression failed with status code $tar_status."

    # Delete the temporary backup file after failed compression
    rm "$temp_backup_filename"
    exit 1
  fi

  # Perform the backup using rsync and capture the exit status
  rsync -a "$temp_backup_filename" "$destination_directory"
  rsync_status=$?

  # Check the exit status to determine if the rsync was successful
  if [ $rsync_status -eq 0 ]; then
    backup_summary+="Backup completed successfully from $source_directory to $destination_directory!"
    echo "$(date +'%b %d %H:%m:%S')"   "Backup completed successfully from $source_directory to $destination_directory!" | tee -a "backup.log"
    

    # Delete the temporary backup file after successful copy
    rm "$temp_backup_filename"
  else
    backup_summary+="Error: Backup failed with status code $rsync_status."
    echo "$(date +'%b %d %H:%m:%S')"  "Error: Backup failed with status code $rsync_status." | tee -a "backup.log"
  fi
}

# Function to delete old backups and retain the specified number
delete_old_backups() {
  local destination_directory=$1
  local num_backups_to_keep=$2

  # Collect a list of current backups
  sorted_backups=($(ls -tr "$destination_directory"/backup_*.tar.gz))
  num_current_backups=${#sorted_backups[@]}

  # Check if there are more backups than the specified number to keep
  if [ "$num_current_backups" -gt "$num_backups_to_keep" ]; then
    # Sort backups by modification time, oldest first
    sorted_backups=($(ls -tr "$destination_directory"/backup_*.tar.gz))
    
    # Calculate the number of backups to delete
    num_backups_to_delete=$((num_current_backups - num_backups_to_keep))

    # Delete the oldest backups
    for ((i = 0; i < num_backups_to_delete; i++)); do
      rm "${sorted_backups[$i]}"
      echo "$(date +'%b %d %H:%m:%S')" "Deleted old backup: ${sorted_backups[$i]}"
    done
  fi
}

# Send email notification
send_email_notification() {
  local email_address="recipient@example.com"
  local subject="Backup Summary"

  # Send email using 'mail' command
  echo "$backup_summary" | mail -s "$subject" "$email_address"

  if [ $? -eq 0 ]; then
    echo "$(date +'%b %d %H:%m:%S')" "Email sent successfully to $email_address." | tee -a "backup.log"
  else
    echo "$(date +'%b %d %H:%m:%S')" "Failed to send email to $email_address." | tee -a "backup.log"
  fi
}

# Prompt the user for the source directory path
read -p "Enter the directory path you want to back up: " source_directory

# Validate the source directory
validate_directory "$source_directory"

# Prompt the user for the backup destination directory path
read -p "Enter the backup destination directory path: " destination_directory

# Create the destination directory if it doesn't exist
create_destination_directory "$destination_directory"

# Get the desired number of backups to keep from the user
read -p "Enter the number of backups to keep: " num_backups

# Validate the desired backups count
validate_backups_number "$num_backups"

# Get the current date and time to use in the backup filename
timestamp=$(date +'%Y%m%d_%H%M%S')

# Generate the temporary backup filename with the timestamp
temp_backup_filename="/tmp/backup_${timestamp}.tar.gz"

# Call the perform_backup function
perform_backup

# Perform backup deletion
delete_old_backups "$destination_directory" "$num_backups"

# Send email notification with the backup summary
send_email_notification