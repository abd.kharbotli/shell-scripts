#!/bin/bash

STOPANDRECONNECT () {
docker stop ${containers[*]}
systemctl restart openvpn-client@lu-ste.prod.surfshark.com_tcp.ovpn
sleep 20
}
STARTCONTAINERS () {
docker start ${containers[*]}
}
COUNTRYCODEAPI () {
countryCodeApi=$(curl --silent https://ipapi.co/country)
}

COUNTRYCODEAPI
res=$?
countryCode="LU"
containers=(crawler1-lu crawler2-lu crawler3-lu effect1-lu)

case $res in
0)
if [ "$countryCodeApi" != "$countryCode" ]; then
echo "VPN is not connected, stopping containers and trying to connect..."
STOPANDRECONNECT
COUNTRYCODEAPI
if [ "$countryCodeApi" = "$countryCode" ]; then
echo "You've become connected, starting containers..."
STARTCONTAINERS
fi
else
STARTCONTAINERS
fi
;;

*)
echo "No Internet, stopping containers and trying to connect..."
STOPANDRECONNECT
COUNTRYCODEAPI
res=$?
if [ $res -eq 0 ] && [ "$countryCodeApi" = "$countryCode" ]; then
echo "You've become connected, starting containers..."
STARTCONTAINERS
fi
;;
esac
