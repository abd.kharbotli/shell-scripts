#########################################################################
#Script Name	: alert-mail-ssc.sh                                                                                              
#Description	: check if root storage is out of space ( more than 85%)                                                               
#Args           : none                                                                                          
#Related Script : none
#########################################################################

#!/bin/bash

output=$(df -H | grep centos-root | awk '{ print $5 " " $1 }')

  echo $output
  usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1  )
  partition=$(echo $output | awk '{ print $2 }' )
  if [ $usep -ge 85 ]; then
    echo "Running out of space \"$partition ($usep%)\" on $(hostname) ip 10.0.50.1 as on $(date) on /root/"  | mail -v -s "Alert-SSC root space" sysadmin@ssc-security.net
#    echo "Alert: Almost out of disk space $usep% on /var/tmpfs (SSC)" | mail -v -s "Alert-SSC tmpfs space" sysadmin@ssc-security.net
  fi
