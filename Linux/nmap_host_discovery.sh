read -p "enter net id: " net
read -p "enter prefix: " prefix

if [[ -d /root/Desktop/scs-abd/Private/$net-$prefix ]]
then
echo -e "\nDirectory /root/Desktop/scs-abd/Private/$net-$prefix already exsits..\n"
else
mkdir /root/Desktop/scs-abd/Private/$net-$prefix
fi

echo -e "\nstarting the host dicovery for $net/$prefix\n"

nmap -sn -n -T4  -PS0-79,81-1000 --max-rate 400 -oG -  $net/$prefix  | grep Up | cut -d ' ' -f2 > /tmp/$net-$prefix-hosts
nmap -sn -n -T4 -PE -oG -  $net/$prefix  | grep Up | cut -d ' ' -f2 >> /tmp/$net-$prefix-hosts

echo -e "\nend of Discovery.\n" 


cat /tmp/$net-$prefix-hosts | sort | uniq >  /root/Desktop/scs-abd/Private/$net-$prefix/Hosts.txt

echo -e "Operation completed successfully.\n"
