#########################################################################
#Script Name    : traccar-info.bash
#Description    : generate info log from "all" traccar log
#Args           : none
#Related Script : none
#########################################################################

#!/bin/bash

dest=/var/log/traccar

tail -f $dest/tracker-server-full.log | grep "INFO:" >> $dest/tracker-server-info.log

