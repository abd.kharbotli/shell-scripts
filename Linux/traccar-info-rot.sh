#########################################################################
#Script Name    : traccar-info-rot.bash
#Description    : traccar log-rotate generator
#Args           : none
#Related Script : none
#########################################################################

#!/bin/bash

dest=/var/log/traccar

dt=$(date --date="yesterday"  +%Y%m%d)

cp $dest/tracker-server-info.log $dest/tracker-server-info.log.$dt

rm -rf $dest/tracker-server-info.log

touch $dest/tracker-server-info.log
