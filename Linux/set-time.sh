#########################################################################
#Script Name    : set-time
#Description    : correct traccar time using monitoring time
#Args           : none
#Related Script : none
#########################################################################

date1=" $(ssh root@10.0.50.2 " date")"

date2=" $(echo  $(date))" 


echo "------------------------------------------------">> /var/log/time.log

echo "set time script starts " >> /var/log/time.log

echo  "traccar date is" , $date2  >> /var/log/time.log


echo "traccar2 date is " , $date1  >> /var/log/time.log


if [ "$date2" != "$date1" ]

 then

 echo "there  is a delay "  >> /var/log/time.log

date -s " $date1 "


fi
