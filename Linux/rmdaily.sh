#########################################################################
#Script Name    : rmdaily
#Description    : delete traccar log older than 15 day
#Args           : none
#Related Script : none
#########################################################################

#!/bin/bash



dest=/var/log/traccar

Date=$(date +%Y%m%d -d "15 days ago")

rm -rf $dest/tracker-server-full.log.$Date
