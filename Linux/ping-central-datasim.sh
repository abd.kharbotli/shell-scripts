#########################################################################
#Script Name    : Ping central datasim
#Description    : check central datasim connection 
#Args           : none
#Related Script : none
#########################################################################


#!/bin/bash
dt=$(date)
echo "------------------------------------------------">> /var/log/ping-sim.log
echo $dt , "SIM check script Start" >> /var/log/ping-sim.log
ping -c 1 192.168.3.1
while :
do
  if ping -c 1 10.9.121.1 &>> /var/log/ping-sim.log
   then
    echo $dt , "SIM is online" >> /var/log/ping-sim.log

  fi

  sleep 600
 break

 echo "LOCAL SIM MODEM IS OFFLINE" >> /var/log/ping-sim.log
done

