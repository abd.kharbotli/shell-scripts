read -p "enter net id: " net
read -p "enter prefix: " prefix

if [[ -d /root/Desktop/scs-abd/Private/$net-$prefix ]]
then
echo -e "\nDirectory /root/Desktop/scs-abd/Private/$net-$prefix already exsits..\n"
else
mkdir /root/Desktop/scs-abd/Private/$net-$prefix
fi

echo -e "\nstarting the host dicovery for $net/$prefix\n"

nmap -sn -n  -PS0-79,81-100 --max-rate 400  --max-hostgroup 1 -oG -  $net/$prefix | grep Up | cut -d ' ' -f2 > /tmp/$net-$prefix-hosts

nmap -sn -n  -PE --max-rate 400 --max-hostgroup 1 -oG - $net/$prefix | grep Up | cut -d ' ' -f2 >> /tmp/$net-$prefix-hosts

cat /tmp/$net-$prefix-hosts | sort -u > /root/Desktop/scs-abd/Private/$net-$prefix/Hosts.txt

echo -e "\nend of Discovery.\n" 

echo -e "\nstarting the port scan on hosts: \n$(cat /root/Desktop/scs-abd/Private/$net-$prefix/Hosts.txt)\n"

nmap -Pn -n -p- -sV --version-all -O --max-rate 400 --max-hostgroup 1 -oA /root/Desktop/scs-abd/Private/$net-$prefix/$net-$prefix -iL /root/Desktop/scs-abd/Private/$net-$prefix/Hosts.txt -v

echo -e "\nEnd of Scanning.\n"

xsltproc /root/Desktop/scs-abd/Private/$net-$prefix/$net-$prefix.xml -o /root/Desktop/scs-abd/Private/$net-$prefix/$net-$prefix.html

echo -e "Operation completed successfully.\n"
