#!/bin/bash

images=($(docker images --format {{.Repository}}:{{.Tag}}))
echo -e "\nList of available images:\n${images[@]}"

echo -e "\n1- Export a container image.\n2- Import a container image.\n"

read -p "Choose one of the previous options: " option

case $option in 

1)
read -p "Enter the image name: " image_name
read -p "Enter the image tag: " tag
for element in ${images[*]};
do
if [ "$element" = "$image_name:$tag" ]
then
read -p "The absolute path to where do you wanna store it (e.g. /path/to/the/): " path
if [ -d $path ]
then
	docker save "$image_name:$tag" > $path$image_name-$tag.tar  2>/dev/null
break
else
	echo -e "\nThe path is not correct.\n"
break
fi
fi
done
;;

2)
read -p "Enter the image name: " image_name
read -p "Enter the image tag: " tag
for element in ${images[*]};
do
if [ "$element" = "$image_name:$tag" ]
then
read -p "The absolute path to where the image is stored (e.g. /path/to/the/): " path
if [ -d $path ]
then
	docker load "$image_name:$tag" < "$path$image_name-$tag.tar" 2>/dev/null
break
else
        echo -e "\nThe path is not correct.\n"
break
fi
fi
done
;;

*)
echo -e "\nNot a valid option.\n"
;;
esac
