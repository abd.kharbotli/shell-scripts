#!/bin/bash

countryCode="NL"

RECONNECTINGTOVPN () {
systemctl restart openvpn-client@nl-ams.prod.surfshark.com_tcp.ovpn
sleep 10
}

COUNTRYCODEAPI () {
countryCodeApi=$(curl --silent https://ipapi.co/country) 
}

COUNTRYCODEAPI
if [ "$countryCodeApi" != "$countryCode" ]; then
    echo "You're not connected to "$countryCode", attempting to connect..."
    RECONNECTINGTOVPN
    COUNTRYCODEAPI
    if [ "$countryCodeApi" = "$countryCode" ]; then
    echo "You've become connected to "$countryCode". "
    else
    echo "We'll try again in an hour."
    fi
else
echo "You're connected to "$countryCode". "
fi
