#########################################################################
#Script Name    : get-csv
#Description    : generate requierd CSV File from traccar DB
#Args           : none
#Related Script : none
#########################################################################

#!/bin/bash

max="$(sudo -i -u traccar -H -- psql -d traccar -c "select MAX(id) from tc_users;" | awk 'FNR == 3 ')"
echo "email,company,notification token,user token"
for ((i=1 ; i<=$max ; i++))
	do
		cmd="$(sudo -i -u traccar -H -- psql -d traccar -c "select email,name from tc_users where id=$i" | awk 'FNR == 3 ')"
		cmd2=$(echo $cmd | sed 's/|/,/g' | tr -s '[:blank:]')
		cmd3="$(sudo -i -u traccar -H -- psql -d traccar -c "select token from tc_users where id=$i" | awk 'FNR == 3 ')"
		cmd4=$(echo $cmd3 | sed 's/|/,/g' | tr -s '[:blank:]')
			if [ "$cmd" == "(0 rows)" ]; then
				continue
			fi
		cmd1="$(sudo -i -u traccar -H -- psql -d traccar -c "select attributes from tc_users where id=$i" | awk 'FNR == 3 ' | sed -nr '/"notificationTokens":"/ s/.*"notificationTokens":"([^"]+).*/\1/p')"
			if [ "$cmd1" == "{}" ]; then
				echo "$cmd2,,$cmd4"
				continue
			fi

			if [ -z "$cmd1" ]
			then
				echo "$cmd2,,$cmd4"
				continue
			fi
		echo "$cmd2,\" $cmd1 \",$cmd4"
	done
