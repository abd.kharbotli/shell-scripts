#########################################################################
#Script Name    : autoDBbackup.sh
#Description    : generate traccar DB backup
#Args           : none
#Related Script : none
#########################################################################

#!/bin/bash

export PGPASSWORD="8GcT4#Pm"

TH=$PATH:/bin
DATE=`date +"%Y-%m-%d"`
dest=/var/lib/pgsql/backups/

mkdir -p /var/lib/pgsql/backups/${DATE}
cd /var/lib/pgsql/backups/${DATE}
pg_dump -h localhost -p 5432 -U traccar traccar > traccardb-${DATE}.bak
tar -czvf traccardb-${DATE}.bak.tar.gz traccardb-${DATE}.bak
rm traccardb-${DATE}.bak
sleep 5
find /var/lib/pgsql/backups/  -type d -ctime +15 | xargs rm -rf
echo "Done"
ls -alh ${dest}*
