#Import-Module VMware.VimAutomation.Core

try {
    # Disable SSL certificate verification
    Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false | Out-Null

    # Prompt for the server IP
    $serverIP = Read-Host -Prompt "Enter the server IP or hostname"

    # Prompt for credentials
    $credential = Get-Credential -Message "Enter your credentials for $serverIP"

    # Access the username and password
    $username = $credential.UserName
    $password = $credential.GetNetworkCredential().Password


    Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -Confirm:$false | Out-Null

    # Check if server IP and VM names are provided
    if ([string]::IsNullOrEmpty($serverIP) -or [string]::IsNullOrEmpty($credential)) {
        throw "Server IP and credentials must be provided."
    }

    # Try connecting to the server
    try {
        Connect-VIServer -Server $serverIP -User $username -Password $password -ErrorAction Stop
    } catch {
        Write-Host "Error: $_"
        # Revert SSL configuration to default
        Set-PowerCLIConfiguration -InvalidCertificateAction Prompt -Confirm:$false | Out-Null
        exit 1
    }

    # Connection successful
    Write-Host "Successfully connected to the server: $serverIP"
    # List of VMs on the server
    Get-VM | Select-Object -ExpandProperty Name

    # Prompt for VM names separated by comma
    $vmNamesString = Read-Host -Prompt "Enter the VM names separated by space (e.g., VM1,VM2,VM3)"
    $vmNames = $vmNamesString -split "," | ForEach-Object { $_.Trim() }

    # Check if server IP and VM names are provided
    if ($vmNames.Count -eq 0) {
        throw "VM names must be provided."
    }

    # Loop through each VM and create a snapshot
    foreach ($vmName in $vmNames) {
        $vm = Get-VM -Name $vmName
        if ($vm) {
            $snapshots = Get-Snapshot -VM $vm
            if ($snapshots) {
                $snapshots | ForEach-Object {
                    $_ | Remove-Snapshot -RemoveChildren -Confirm:$false
                    Write-Host "Snapshot $($_.Name) and its children are being removed..."
                }
            } else {
                Write-Host "No snapshots found for $vmName."
            }

            # Wait for snapshots to be deleted
            do {
                Start-Sleep -Seconds 5
                $remainingSnapshots = Get-Snapshot -VM $vm
            } while ($remainingSnapshots)

            
        } else {
                Write-Host "VM $vmName not found."
        }
    }

    # Disconnect from the server when done
    Disconnect-VIServer -Server $serverIP -Confirm:$false
} catch {
    Write-Host "Error: $_"
    # Revert SSL configuration to default
    Set-PowerCLIConfiguration -InvalidCertificateAction Prompt -Confirm:$false | Out-Null
    # Empty Credintials
    Remove-Variable serverIP,credential,username,password
    exit 1
} finally {
    # Revert SSL configuration to default
    Set-PowerCLIConfiguration -InvalidCertificateAction Prompt -Confirm:$false | Out-Null
    # Empty Credintials
    Remove-Variable serverIP,credential,username,password
}